/*

1. What directive is used by Node.js in loading the module it needs?
	- require directive
2. What node.js module contains a method from server creation?
	- http module
3. What is the method of the http object responsible for creating a server using node.js?
	- createServer
4. What method of the response object allows us to set status codes and content types.
	- response.writeHead() 
5. Where will console.log() output its content when run in node.js 
	- It will be render on the localhost and port that you created or in the server that you created.

6. What property of the request object contains the address's point?
	request.url 

*/
