
const http = require('http');
const port = 3000

http.createServer((req, res) => {
	if(req.url == '/login'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Login Page');
	}else{
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end(`I'm sorry the you are looking for cannot be found`);		
	}
}).listen(port);

console.log(`The server is successfully running`);